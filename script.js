/**
 * 1. ilk N natural ədədin cəmi
 */
const N = 10;

let sum = 0;
for (let i = 1; i <= N; i++) {
  sum = sum + i;
}

console.log(`1-dən ${N}-ə qədər ədədlərin cəmi: ${sum}`);

/**
 * 2. 1-dən n-ə qədər neçə sadə ədəd olduğunu tap
 */
let count = 0;
for (let number = 2; number <= N; number++) {
  let isPrime = true;
  for (let k = 2; k < number; k++) {
    if (number % k == 0) {
      isPrime = false;
      break;
    }
  }
  if (isPrime) {
    count++;
  }
}

console.log(`1-dən ${N}-ə qədər ${count} sadəd ədəd var.`);

/**
 * 3. n ədədinin faktorialını tap
 */
let factorial = 1;
for (let i = 1; i <= N; i++) {
  factorial = factorial * i;
}
console.log(`${N}! = ${factorial}`);

/**
 * 5. Diyez işarələrindən ibarət nəticə
 */
let resultHash = '';
for (let i = 1; i <= N; i++) {
  for (let j = 1; j <= N; j++) {
    resultHash = resultHash + '# ';
  }
  resultHash = resultHash + '\n'; // new line character
}
console.log(resultHash);

/**
 * 6. Ulduz işarələrindən ibarət üçbucaq fason nəticə
 */
let resultStar = '';
for (let i = 1; i <= N; i++) {
  for (let j = 1; j <= i; j++) {
    resultStar = resultStar + '* ';
  }
  resultStar = resultStar + '\n'; // new line character
}
console.log(resultStar);

/**
 * 9. Floyd üçbucağı
 */
let floydTriangle = '';
let currentNumber = 1;
for (let i = 1; i <= N; i++) {
  for (let j = 1; j <= i; j++) {
    floydTriangle = floydTriangle + currentNumber + ' ';
    currentNumber++;
  }
  floydTriangle = floydTriangle + '\n'; // new line character
}
console.log(floydTriangle);

/**
 * 11. İkilik say sistemindən onluq say sisteminə
 */
let binaryNumber = 10100101;

let decimalNumber = 0;
let index = 0;
while (binaryNumber > 0) {
  const lastDigit = binaryNumber % 10;
  decimalNumber += lastDigit * Math.pow(2, index);

  binaryNumber = Math.floor(binaryNumber / 10);
  index++;
}

console.log(decimalNumber);

/**
 * 8. ilk n fibonaççi ədədini çap edin
 */

let first = 1;
let second = 1;

console.log(`1-ci fibonaççi ədədi ${first}`);
console.log(`2-ci fibonaççi ədədi ${second}`);

for (let i = 1; i <= N; i++) {
  const current = first + second;
  console.log(`${i + 2}-ci fibonaççi ədədi ${current}`);
  first = second;
  second = current;
}
